package com.springboot.api.gradle.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.gradle.dao.impl.ProductoDaoImpl;
import com.springboot.api.gradle.model.Producto;
import com.springboot.api.gradle.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductoDaoImpl _productoDao;
	
	@Override
	public List<Producto> getAllProductos() {
		System.out.println("ProductoServiceImpl.getAllProductos");
		return _productoDao.getAllProductos();
	}

	@Override
	public Producto getProducto(Integer id) {
		
		return _productoDao.getProducto(id);
	}

	@Override
	public void saveProducto(Producto persona) {
		try {
			_productoDao.saveProducto(persona);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}

	@Override
	public void deleteProducto(Integer id) {
		try {
			_productoDao.deleteProducto(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}

}
