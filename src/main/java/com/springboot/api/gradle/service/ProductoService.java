package com.springboot.api.gradle.service;

import java.util.List;

import com.springboot.api.gradle.model.Producto;

public interface ProductoService {
	
	
	void saveProducto(Producto producto);
	List<Producto> getAllProductos();
	Producto getProducto(Integer id);
	void deleteProducto(Integer id);
	
}
