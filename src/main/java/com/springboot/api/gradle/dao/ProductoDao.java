package com.springboot.api.gradle.dao;

import java.util.List;

import com.springboot.api.gradle.model.Producto;

public interface ProductoDao {
	
	List<Producto> getAllProductos();
	Producto getProducto(Integer id);
	void saveProducto(Producto producto);
	void deleteProducto(Integer id);

}
