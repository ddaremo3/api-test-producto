package com.springboot.api.gradle.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.gradle.dao.ProductoDao;
import com.springboot.api.gradle.model.Persona;
import com.springboot.api.gradle.model.Producto;
import com.springboot.api.gradle.rowmapper.PersonaRowMapper;
import com.springboot.api.gradle.rowmapper.ProductoRowMapper;

@Repository
public class ProductoDaoImpl extends JdbcDaoSupport implements ProductoDao {

	public ProductoDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	
	@Override
	public void saveProducto(Producto producto) {
		
		String sql = "insert into api_test.producto (id, descripcion, categoria, precio_unitario, stock_anual, stock_minimo, estado) "  
				+ "values (?, ?, ?, ?, ?, ?, ?);";
		
		Object[] params = {producto.getId(), producto.getDescripcion(), producto.getCategoria(), producto.getPrecioUnitario(), producto.getStockActual(), producto.getStockMinimo(), producto.getEstado()};
		int[] tipos = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado el producto "+producto.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}
	
	
	@Override
	public List<Producto> getAllProductos() {
		logger.debug("::::: Mensaje de prueba :::::::");
		System.out.println("ProductoDaoImpl.getAllProductos - Inic");
		List<Producto> listaProductos = new ArrayList<Producto>();
		
		String sql = " SELECT id, descripcion, categoria, precio_unitario, stock_anual, stock_minimo, estado \n" + 
				" FROM api_test.producto";
		
		try {
			
			RowMapper<Producto> productoRow = new ProductoRowMapper();
			listaProductos = getJdbcTemplate().query(sql, productoRow);
			logger.debug("Se han listado "+listaProductos.size()+" productos");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		System.out.println("ProductoDaoImpl.getAllProductos - Fin : " + listaProductos);
		
		return listaProductos;
	}

	@Override
	public Producto getProducto(Integer id) {
		logger.debug("::::: Mensaje de prueba :::::::");
		Producto producto = new Producto();	
		List<Producto> listaProductos = new ArrayList<Producto>();
		
		String sql = " SELECT id, descripcion, categoria, precio_unitario, stock_anual, stock_minimo, estado\n" + 
				" FROM api_test.producto where id='"+id+"'";
				
		try {
			
			RowMapper<Producto> productoRow = new ProductoRowMapper();
			listaProductos = getJdbcTemplate().query(sql, productoRow);
			
			producto = listaProductos.get(0);
			
			logger.debug("Se ha traido el producto "+producto.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return producto;
	}

	

	@Override
	public void deleteProducto(Integer id) {
		int regeliminados = 0;		
		String sql = " delete from producto where id ='"+id+"'";		
		try {			
			regeliminados = getJdbcTemplate().update(sql);
			logger.debug("Se han eliminado "+regeliminados+" productos con id = "+id);
		} catch (Exception e) {			
			logger.error(e.getMessage());
		}
	}

}
