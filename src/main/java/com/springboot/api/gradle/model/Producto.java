package com.springboot.api.gradle.model;

import java.math.BigDecimal;

public class Producto {
	
	private Integer id;
	private String descripcion;
	private String categoria;
	private Integer precioUnitario;
	private Integer stockActual;
	private Integer stockMinimo;
	private Integer estado;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Integer getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(Integer precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public Integer getStockActual() {
		return stockActual;
	}

	public void setStockActual(Integer stockActual) {
		this.stockActual = stockActual;
	}

	public Integer getStockMinimo() {
		return stockMinimo;
	}

	public void setStockMinimo(Integer stockMinimo) {
		this.stockMinimo = stockMinimo;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}


	@Override
	public String toString() {
		return "Producto [id=" + id + ", descripcion=" + descripcion + ", categoria=" + categoria + ", precioUnitario="
				+ precioUnitario + ", stockActual=" + stockActual + ", stockMinimo=" + stockMinimo + ", estado="
				+ estado + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
}
