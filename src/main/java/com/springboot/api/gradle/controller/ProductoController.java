package com.springboot.api.gradle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Producto;
import com.springboot.api.gradle.service.impl.ProductoServiceImpl;

@RestController
@RequestMapping("/producto")
public class ProductoController {
	
	@Autowired
	private ProductoServiceImpl _productoService;
	
	@GetMapping( produces = "application/json")	
	public List<Producto> getAllProductos(){
		return _productoService.getAllProductos();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")	
	public Producto getProducto(@PathVariable ("id") Integer id){
		return _productoService.getProducto(id);
	}
	
	@PostMapping(produces = "application/json")	
	public List<Producto> saveProducto(@RequestBody Producto producto){
		
		_productoService.saveProducto(producto);
		
		return _productoService.getAllProductos();
	}	
	
	@DeleteMapping(value = "/{id}", produces = "application/json")	
	public List<Producto> deleteProducto(@PathVariable ("id") Integer id){
		
		_productoService.deleteProducto(id);
		
		return _productoService.getAllProductos();
	}	

}
