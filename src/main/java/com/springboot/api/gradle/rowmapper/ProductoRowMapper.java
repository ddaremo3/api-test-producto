package com.springboot.api.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.gradle.model.Producto;

public class ProductoRowMapper implements RowMapper<Producto>{

	@Override
	public Producto mapRow(ResultSet rs, int rowNum) throws SQLException {
		Producto producto = new Producto();
		
		producto.setId(rs.getInt("id"));
		producto.setDescripcion(rs.getString("descripcion"));
		producto.setCategoria(rs.getString("categoria"));
		producto.setPrecioUnitario(rs.getInt("precio_unitario"));
		producto.setStockActual(rs.getInt("stock_anual"));
		producto.setStockMinimo(rs.getInt("stock_minimo"));
		producto.setEstado(rs.getInt("estado"));
		
		return producto;
	}

}
